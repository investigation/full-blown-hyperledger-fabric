# Full-blown Hyperledger Fabric

This project aims to provide a simple step-by-step guide to setup a test Hyperledger Fabric (HLF) network without utilizing the exisitng [official test-network](https://hyperledger-fabric.readthedocs.io/en/release-2.2/test_network.html).

The studied test-network consists of 1 Consortioum Member (single Organization - **TestCorp**) and the Ordering Service Organization (assumed independent - OrderOrg).

The network componets are the following:
- tls CA + root CA
- peer1.testCorp.ca1
- orderer.orderOrg.ca1

<hr>

Required Steps:
1. Setup the CAs
    1. download the fabric-ca binaries *(ca-server + ca-client)*
    2. Create the **TLS CA server** and call from **tls-ca-client**
        - docker-compose up ca-tls [source](https://hyperledger-fabric-ca.readthedocs.io/en/latest/operations_guide.html#setup-tls-ca)
        - enroll TLS CA Admin
        - issue TLS Certificates to **ALL** network communicating nodes (peer1-org1, orderer1-org0)
    3. Setup Ordering Org CA
        - fire up the CA server (docker-compose up -d rca-org0)
        - enroll admin and register orderer1 nodes (orderer1, orderer-admin)
    4. Setup Consortium Orgs CAs (per consortium member)
        - fire up the CA server (docker-compose up -d rca-org1)
        - enroll admin and register org nodes (peer1, admin1-org1, user-org1)
    5. Start enrolling Peers.
        - fire up the CA server (docker-compose up -d rca-org1)
        - enroll admin and register org nodes (peer1, admin1-org1, user-org1)
    6. Setup Ordering Nodes.
        - enroll admin and orderer
        - configure the configtx.yaml to create the genesis block and the channel.tx files.
    7. Create CLI Containers (uses an existing org peer).
        - just a docker-compose service
    8. User Org1-CLI to create a new channel using the genesis block and the channel.tx outouts of the configtxgen command on the orderer.
    9. Join the create channel from all peers (peer1-org1)
    10. Install and Instantiate Chaincode (exec from org-cli container)
        - `peer chaincode install ...`
        - `peer chaincode instantiate ...`
    11. Invoke and Query Chaincode (exec from org-cli container)
        - `peer chaincode query ...`
        - `peer chaincode invoke ...`



# TODOs

Questions to answer:
1. Is it possible to enroll end-users and org peers without cli (maybe through fabric sdk)?
2. Is it possible to query/invoke chaincode without cli (through fabric sdk)?
3. Organize docker-compose services to k8s namespace, services and pods.
